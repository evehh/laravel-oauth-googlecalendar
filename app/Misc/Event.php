<?php

namespace App\Misc;

use Carbon\Carbon;

class Event
{
    public static function remove($service,$data){
        $service->events->delete($data->calendarId, $data->eventId);
        return true;
    }

    public static function getAll($service,$request)
    {
        $results = [];
        $calId = $request->calendarId;
        $requestType = $request->type;
        $opt = array('timeMin' => Carbon::now()->subWeeks(4)->toAtomString());
        $entries = $service->events->listEvents($calId,$opt);
        while(true) {
            foreach ($entries->getItems() as $event) {
                $singleEntry = [];
                $singleEntry["id"] = $event->id;
                $singleEntry["calendarId"] = $calId;
                $singleEntry["summary"] = $event->summary;
                $singleEntry["description"] = $event->description;
                $singleEntry["startDate"] = $event->start->date;
                $singleEntry["startDateTime"] = $event->start->dateTime;
                $singleEntry["startDateTimeTz"] = $event->start->timeZone;
                $singleEntry["endDateTime"] = $event->end->dateTime;
                $singleEntry["endDate"] = $event->end->date;
                $singleEntry["endDateTimeTz"] = $event->end->timeZone;
                if($requestType == 'web'){
                    $singleEntry["carbonStartDate"] = $event->start->date ? Carbon::parse($event->start->date)->toDateString() : null;
                    $singleEntry["carbonStartDateTime"] = $event->start->dateTime ? Carbon::parse($event->start->dateTime)->toDateTimeString() : null;
                    $singleEntry["carbonEndDate"] = $event->end->date ? Carbon::parse($event->end->date)->toDateString() : null;
                    $singleEntry["carbonEndDateTime"] = $event->end->dateTime ? Carbon::parse($event->end->dateTime)->toDateTimeString() : null;
                    $singleEntry["singleEventUrl"] = url('/events/'.$calId.'/'.$event->id);
                    $singleEntry["deleteEventUrl"] = url('/api/events/delete');
                }
                array_push($results,$singleEntry);
            }
            $pageToken = $entries->getNextPageToken();
            if ($pageToken) {
                $opt["pageToken"] = $pageToken;
                $entries = $service->events->listEvents($calId, $opt);
            } else {
                break;
            }
        }
        return $results;
    }

    public static function getEvent($service,$request)
    {
        $results = [];
        $calId = $request->calendarId;
        $eventId = $request->eventId;
        $requestType = $request->type;
        $event = $service->events->get($calId,$eventId);
        $results["id"] = $event->id;
        $results["calendarId"] = $calId;
        $results["summary"] = $event->summary;
        $results["description"] = $event->description;
        $results["startDate"] = $event->start->date;
        $results["startDateTime"] = $event->start->dateTime;
        $results["startDateTimeTz"] = $event->start->timeZone;
        $results["endDateTime"] = $event->end->dateTime;
        $results["endDate"] = $event->end->date;
        $results["endDateTimeTz"] = $event->end->timeZone;
        $results["endTimeUnspecified"] = $event->endTimeUnspecified;
        if($requestType == 'web'){
            $results["carbonStartDate"] = $event->start->date ? Carbon::parse($event->start->date)->format('Y-m-d\TH:i') : null;
            $results["carbonStartDateTime"] = $event->start->dateTime ? Carbon::parse($event->start->dateTime)->format('Y-m-d\TH:i') : null;
            $results["carbonEndDate"] = $event->end->date ? Carbon::parse($event->end->date)->format('Y-m-d\TH:i') : null;
            $results["carbonEndDateTime"] = $event->end->dateTime ? Carbon::parse($event->end->dateTime)->format('Y-m-d\TH:i') : null;
        }
        return $results;
    }

    public static function newGoogleCalendarEvent($data){
        $model = [];
        $tz = 'America/Santiago';
        $model["summary"] = $data->summary;
        $model["description"] = $data->description;

        if($data->endDateTime){
            $model["start"]["dateTime"] = Carbon::parse($data->startDateTime)->shiftTimezone($tz);
            $model["start"]["timeZone"] = $tz;
            $model["end"]["dateTime"] = Carbon::parse($data->endDateTime)->shiftTimezone($tz);
            $model["end"]["timeZone"] = $tz;
        }else{
            $model["start"]["date"] = Carbon::parse($data->startDateTime)->format('Y-m-d');
            $model["start"]["timeZone"] = $tz;
            $model["end"]["date"] = Carbon::parse($data->startDateTime)->format('Y-m-d');
            $model["end"]["timeZone"] = $tz;
        }
        return new \Google_Service_Calendar_Event($model);
    }


}
