<?php

namespace App\Misc;

use Carbon\Carbon;

class Calendar
{

    public static function getAll($service,$request)
    {
        $results = [];
        $optParams = array('minAccessRole' => 'owner');
        $calendarList = $service->calendarList->listCalendarList($optParams);
        $results = [];
        while(true) {
            foreach ($calendarList->getItems() as $calendarListEntry) {
                $singleCalendar = [];
                $singleCalendar["id"] = $calendarListEntry->id;
                $singleCalendar["summary"] = $calendarListEntry->summary;
                $singleCalendar["summaryOverride"] = $calendarListEntry->summaryOverride;
                if($request->type == 'web'){
                    $singleCalendar["singleCalendarUrl"] = url('events/'.$calendarListEntry->id);
                }
                array_push($results,$singleCalendar);
            }
            $pageToken = $calendarList->getNextPageToken();
            if ($pageToken) {
                $optParams['pageToken'] = $pageToken;
                $calendarList = $service->calendarList->listCalendarList($optParams);
            } else {
                break;
            }
        }
        return $results;
    }


}
