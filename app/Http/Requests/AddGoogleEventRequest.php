<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AddGoogleEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => ['bail','required', 'string', Rule::in(['web','api'])],
            'accessToken' => ['bail','required', 'string'],
            'calendarId' => ['bail','required','string'],
            'summary' => ['bail','required', 'string'],
            'description' => ['bail','nullable', 'string'],
            'startDateTime' => ['bail','required', 'date'],
            'endDateTime' => ['bail','nullable', 'date','after:startDateTime']
        ];
    }
}
