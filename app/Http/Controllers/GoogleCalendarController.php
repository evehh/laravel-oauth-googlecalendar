<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Facades
use Auth;
use Exception;
use Log;
use Socialite;
use Storage;
use URL;

//Models
use App\Models\User;


class GoogleCalendarController extends Controller
{
    public function index(){
        return view('calendars.index');
    }

    public function events($calendarId){
        return view('calendars.events.list')->with([
            'calendarId' => $calendarId
        ]);
    }

    public function eventAdd($calendarId){
        return view('calendars.events.add')->with([
            'calendarId' => $calendarId
        ]);
    }

    public function event($calendarId,$eventId){
        return view('calendars.events.details')->with([
            'calendarId' => $calendarId,
            'eventId' => $eventId
        ]);
    }

}
