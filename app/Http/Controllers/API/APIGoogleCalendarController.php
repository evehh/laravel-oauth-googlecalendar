<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ListGoogleCalendarRequest;
use App\Http\Requests\ListGoogleEventsRequest;
use App\Http\Requests\AddGoogleEventRequest;
use App\Http\Requests\RemoveGoogleEventRequest;
use App\Http\Requests\GetGoogleEventRequest;
use App\Http\Requests\UpdateGoogleEventRequest;

//Helpers
use Log;
use App\Exceptions\Handler;
use App\Misc\Google;
use App\Misc\Event;
use App\Misc\Calendar;

//Errors
use Error;
use Exception;

class APIGoogleCalendarController extends Controller
{
    public function listCalendars(ListGoogleCalendarRequest $request){
        try {
            $google = new Google;
            $service = $google->connectUsing($request->accessToken)->service('Calendar');
            $results = Calendar::getAll($service,$request);
            return response(['status'=>'success','calendars' => $results], 200);
        } catch (Exception $th) {
            Log::critical("CUSTOM Exception");
            $h = new Handler(app());
            $h->report($th);
            return response(['type' => 'toast', 'error' => 'Ha ocurrido un error inesperado. Actualice la página e intente nuevamente.'], 400);
        } catch (Error $th) {
            Log::critical("CUSTOM Error");
            $h = new Handler(app());
            $h->report($th);
            return response(['type' => 'toast', 'error' => 'Ha ocurrido un error inesperado. Actualice la página e intente nuevamente.'], 400);
        }
    }

    public function listEvents(ListGoogleEventsRequest $request){
        try {
            $google = new Google;
            $service = $google->connectUsing($request->accessToken)->service('Calendar');
            $results = Event::getAll($service,$request);
            return response(['status'=>'success','events' => $results], 200);
        } catch (Exception $th) {
            Log::critical("CUSTOM Exception");
            $h = new Handler(app());
            $h->report($th);
            return response(['type' => 'toast', 'error' => 'Ha ocurrido un error inesperado. Actualice la página e intente nuevamente.'], 400);
        } catch (Error $th) {
            Log::critical("CUSTOM Error");
            $h = new Handler(app());
            $h->report($th);
            return response(['type' => 'toast', 'error' => 'Ha ocurrido un error inesperado. Actualice la página e intente nuevamente.'], 400);
        }
    }

    public function addEvent(AddGoogleEventRequest $request){
        try {
            $google = new Google;
            $service = $google->connectUsing($request->accessToken)->service('Calendar');
            $event = Event::newGoogleCalendarEvent($request);
            $service->events->insert($request->calendarId,$event);
            return response(['status'=>'success'], 200);
        } catch (Exception $th) {
            Log::critical("CUSTOM Exception");
            $h = new Handler(app());
            $h->report($th);
            return response(['type' => 'toast', 'error' => 'Ha ocurrido un error inesperado. Actualice la página e intente nuevamente.'], 400);
        } catch (Error $th) {
            Log::critical("CUSTOM Error");
            $h = new Handler(app());
            $h->report($th);
            return response(['type' => 'toast', 'error' => 'Ha ocurrido un error inesperado. Actualice la página e intente nuevamente.'], 400);
        }
    }

    public function updateEvent(UpdateGoogleEventRequest $request){
        try {
            $google = new Google;
            $service = $google->connectUsing($request->accessToken)->service('Calendar');
            $event = Event::newGoogleCalendarEvent($request);
            $service->events->update($request->calendarId,$request->eventId,$event);
            return response(['status'=>'success','message'=>'Evento modificado exitosamente.'], 200);
        } catch (Exception $th) {
            Log::critical("CUSTOM Exception");
            $h = new Handler(app());
            $h->report($th);
            return response(['type' => 'toast', 'error' => 'Ha ocurrido un error inesperado. Actualice la página e intente nuevamente.'], 400);
        } catch (Error $th) {
            Log::critical("CUSTOM Error");
            $h = new Handler(app());
            $h->report($th);
            return response(['type' => 'toast', 'error' => 'Ha ocurrido un error inesperado. Actualice la página e intente nuevamente.'], 400);
        }
    }



    public function event(GetGoogleEventRequest $request){
        try {
            $google = new Google;
            $service = $google->connectUsing($request->accessToken)->service('Calendar');
            $event = Event::getEvent($service,$request);
            return response(['status'=>'success','event' => $event], 200);
        } catch (Exception $th) {
            Log::critical("CUSTOM Exception");
            $h = new Handler(app());
            $h->report($th);
            return response(['type' => 'toast', 'error' => 'Ha ocurrido un error inesperado. Actualice la página e intente nuevamente.'], 400);
        } catch (Error $th) {
            Log::critical("CUSTOM Error");
            $h = new Handler(app());
            $h->report($th);
            return response(['type' => 'toast', 'error' => 'Ha ocurrido un error inesperado. Actualice la página e intente nuevamente.'], 400);
        }
    }

    public function removeEvent(RemoveGoogleEventRequest $request){
        try {
            $google = new Google;
            $service = $google->connectUsing($request->accessToken)->service('Calendar');
            Event::remove($service,$request);
            return response(['status'=>'success','removed'=>true,'message'=>'Evento eliminado exitosamente.','eventId'=>$request->eventId], 200);
        } catch (Exception $th) {
            Log::critical("CUSTOM Exception");
            $h = new Handler(app());
            $h->report($th);
            return response(['type' => 'toast', 'error' => 'Ha ocurrido un error inesperado. Actualice la página e intente nuevamente.'], 400);
        } catch (Error $th) {
            Log::critical("CUSTOM Error");
            $h = new Handler(app());
            $h->report($th);
            return response(['type' => 'toast', 'error' => 'Ha ocurrido un error inesperado. Actualice la página e intente nuevamente.'], 400);
        }
    }

}

