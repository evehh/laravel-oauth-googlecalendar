<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

//Facades
use Auth;
use Exception;
use Log;
use Socialite;
use Storage;
use URL;

//Models
use App\Models\User;

//Google

class GoogleSocialiteController extends Controller
{

    public function redirectToGoogle()
    {
        return Socialite::driver('google')
        ->scopes([
            'https://www.googleapis.com/auth/calendar',
            'openid',
            'profile',
            'email',
            'https://www.googleapis.com/auth/contacts.readonly'
            ]) // For any extra scopes you need, see https://developers.google.com/identity/protocols/googlescopes for a full list; alternatively use constants shipped with Google's PHP Client Library
        ->with(["access_type" => "offline", "prompt" => "select_account consent"])
        ->redirect();


    }

    public function handleCallback()
    {
        try {
            $user = Socialite::driver('google')->user();

            //$user = Socialite::driver('google')->user();
            //$user = $socialite->user();
            //dd($socialite->user());

            $finduser = User::where('social_id', $user->id)->first();
            //$userFromToken = Socialite::driver('google')->userFromToken($token);
            //dd($userFromToken);
            if($finduser){
                Auth::login($finduser);
                $finduser->social_id = $user->id;
                $finduser->social_token  = $user->token;
                $finduser->social_refresh_token  = $user->refreshToken;
                $finduser->social_expires_in  = $user->expiresIn;
                $finduser->save();
                return redirect('/dashboard');

            }else{
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'social_id'=> $user->id,
                    'social_token' => $user->token,
                    'social_refresh_token' => $user->refreshToken,
                    'social_expires_in' => $user->expiresIn,
                    'social_type'=> 'google',
                    'password' => encrypt('my-google')
                ]);

                Auth::login($newUser);

                return redirect('/dashboard');
            }

        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
}
