<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Calendarios > Eventos') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <a style="border:1px solid #3c3c3c; padding: 5px 5px 5px 5px;margin-bottom:15px;" href="{{url('/events/'.$calendarId.'/add')}}">Nuevo evento</a>
                    <br><br>
                    <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                        <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" class="px-6 py-3">ID</th>
                                <th scope="col" class="px-6 py-3">Summary</th>
                                <th scope="col" class="px-6 py-3">Start</th>
                                <th scope="col" class="px-6 py-3"><span class="sr-only">Acciones</span></th>
                            </tr>
                        </thead>
                        <tbody id="eventsTableBody">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<script src="{{mix('js/calendars/events/list.js')}}"></script>
<script>
    let AT = '{!!Auth::user()->getAccessToken()!!}';
    document.addEventListener("DOMContentLoaded",()=>{
        _eventsList(AT,'{{route("api_events")}}','{{$calendarId}}');
    });
</script>
