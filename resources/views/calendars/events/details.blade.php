<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Calendarios > Eventos > Detalles') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form class="w-full max-w-xl" id="addEventForm">
                        <div class="md:flex md:items-center mb-6">
                            <div class="md:w-1/3">
                                <label class="block text-gray-500 font-bold md:text-right mb-2 md:mb-0 pr-4"
                                    for="inline-full-name">
                                    Summary
                                </label>
                            </div>
                            <div class="md:w-2/3">
                                <input
                                    class="mb-4 bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                                    type="text" value="" name="summary" required>
                            </div>
                        </div>
                        <div class="md:flex md:items-center mb-6">
                            <div class="md:w-1/3">
                                <label class="block text-gray-500 font-bold md:text-right mb-2 md:mb-0 pr-4"
                                    for="inline-full-name">
                                    Description
                                </label>
                            </div>
                            <div class="md:w-2/3">
                                <textarea
                                    class="mb-4 bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                                    name="description"></textarea>
                            </div>
                        </div>
                        <div class="md:flex md:items-center mb-6">
                            <div class="md:w-1/3">
                                <label class="block text-gray-500 font-bold md:text-right mb-2 md:mb-0 pr-4"
                                    for="inline-full-name">
                                    StartDateTime
                                </label>
                            </div>
                            <div class="md:w-2/3">
                                <input
                                    class="mb-4 bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                                    type="datetime-local" value="" name="startDateTime" required>
                            </div>
                        </div>
                        <div class="md:flex md:items-center mb-6">
                            <div class="md:w-1/3">
                                <label class="block text-gray-500 font-bold md:text-right mb-2 md:mb-0 pr-4"
                                    for="inline-full-name">
                                    EndDateTime
                                </label>
                            </div>
                            <div class="md:w-2/3">
                                <input
                                    class="mb-4 bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                                    type="datetime-local" value="" name="endDateTime">
                            </div>
                        </div>
                        <div class="md:flex md:items-center mb-6">
                            <div class="md:w-1/3">
                                <button
                                    style="border:1px solid #3c3c3c; border-radius:5px; background-color:olivedrab; padding: 5px 15px 5px 15px;margin-bottom:15px;"
                                    type="submit">Modificar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<script src="{{mix('js/calendars/events/details.js')}}"></script>
<script>
    document.addEventListener("DOMContentLoaded",()=>{
        _eventDetails('{!!Auth::user()->getAccessToken()!!}','{{route("api_event")}}','{{$calendarId}}','{{$eventId}}');
        let addEventForm = document.getElementById("addEventForm");
        addEventForm.addEventListener("submit",(e)=>{
            e.preventDefault();
            _eventUpdate('{!!Auth::user()->getAccessToken()!!}','{{route("api_events_update")}}','{{$calendarId}}','{{$eventId}}');
        });
    });
</script>
