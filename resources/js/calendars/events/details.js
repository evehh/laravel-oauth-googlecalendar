_eventDetails = function(accessToken,route, calendarId, eventId){
    let method = "POST";
    let url = route;
    let data = {};
    data["accessToken"] = accessToken;
    data["type"] = 'web';
    data["calendarId"] = calendarId;
    data["eventId"] = eventId;
    let xhrParams = {
        "method":method,
        "url":url,
        "data":data
    };
    _custom_XHRexec(xhrParams,true);
}

_eventUpdate = function(accessToken, route, calendarId, eventId) {
  var method = "POST";
  var url = route;
  var data = {};
  var summaryInput = document.querySelector("input[name='summary']");
  var descriptionInput = document.querySelector("textarea[name='description']");
  var startDateTimeInput = document.querySelector("input[name='startDateTime']");
  var endDateTimeInput = document.querySelector("input[name='endDateTime']");
  data["accessToken"] = accessToken;
  data["type"] = 'web';
  data["calendarId"] = calendarId;
  data["eventId"] = eventId;
  data["summary"] = summaryInput.value;
  data["description"] = descriptionInput.value;
  data["startDateTime"] = startDateTimeInput.value;
  data["endDateTime"] = endDateTimeInput.value;
  var xhrParams = {
    "method": method,
    "url": url,
    "data": data
  };

  _custom_XHRexec(xhrParams, true);
};


_custom_successXHR_execution = function(jsonResponse){
    if(jsonResponse.hasOwnProperty("event")){
        let summaryInput = document.querySelector("input[name='summary']");
        let descriptionInput = document.querySelector("textarea[name='description']");
        let startDateTimeInput = document.querySelector("input[name='startDateTime']");
        let endDateTimeInput = document.querySelector("input[name='endDateTime']");
        let startDateValue = jsonResponse.event.carbonStartDateTime ? jsonResponse.event.carbonStartDateTime : jsonResponse.event.carbonStartDate
        let endDateValue = jsonResponse.event.carbonEndDateTime ? jsonResponse.event.carbonEndDateTime : jsonResponse.event.carbonEndDate;
        summaryInput.value= jsonResponse.event.summary;
        descriptionInput.value= jsonResponse.event.description;
        startDateTimeInput.value= startDateValue;
        endDateTimeInput.value= startDateValue == endDateValue ? null : endDateValue;
    }else{
        _toast_statusMessage('success',jsonResponse.message);
    }
}
