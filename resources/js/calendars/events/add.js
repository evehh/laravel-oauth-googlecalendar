_eventAdd = function(accessToken,route,calendarId){
    let method = "POST";
    let url = route;
    let data = {};
    let summaryInput = document.querySelector("input[name='summary']");
    let descriptionInput = document.querySelector("textarea[name='description']");
    let startDateTimeInput = document.querySelector("input[name='startDateTime']");
    let endDateTimeInput = document.querySelector("input[name='endDateTime']");

    data["accessToken"] = accessToken;
    data["type"] = 'web';
    data["calendarId"] = calendarId;
    data["summary"] = summaryInput.value;
    data["description"] = descriptionInput.value;
    data["startDateTime"] = startDateTimeInput.value;
    data["endDateTime"] = endDateTimeInput.value;

    let xhrParams = {
        "method":method,
        "url":url,
        "data":data
    };

    _custom_XHRexec(xhrParams,true);
}
