_eventsList = function(accessToken,route, calendarId){
    let method = "POST";
    let url = route;
    let data = {};
    data["accessToken"] = accessToken;
    data["type"] = 'web';
    data["calendarId"] = calendarId;
    let xhrParams = {
        "method":method,
        "url":url,
        "data":data
    };
    _custom_XHRexec(xhrParams,true);
}

_eventsDelete = function(accessToken, route, calendarId, eventId){
        Swal.fire({
            title: "Confirmar",
            text: "¿Desea eliminar el evento (ID "+eventId+")?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: "Confirmar",
            cancelButtonText: "Cancelar",
        })
        .then(resultado => {
            if (resultado.value) {
                let method = "POST";
                let url = route;
                let data = {};
                data["accessToken"] = accessToken;
                data["type"] = 'web';
                data["calendarId"] = calendarId;
                data["eventId"] = eventId;
                let xhrParams = {
                    "method":method,
                    "url":url,
                    "data":data
                };
                _custom_XHRexec(xhrParams,true);
            }
        });
    }

_custom_successXHR_execution = function(jsonResponse){
    if(jsonResponse.hasOwnProperty("events") && jsonResponse.events.length > 0){
        let contentToAppend = ``;
        _delete_element('eventsTableBody','clear');
        jsonResponse.events.forEach((element) => {
            let startDate = element.carbonStartDateTime ? element.carbonStartDateTime : element.carbonStartDate;
            contentToAppend += `
            <tr id="tr_`+element.id+`">
                <td class="px-6 py-4" style="word-break: break-word;">`+element.id+`</td>
                <td class="px-6 py-4" style="word-break: break-word;">`+element.summary+`</td>
                <td class="px-6 py-4" style="word-break: break-word;">`+startDate+`</td>
                <td class="px-6 py-4" style="word-break: break-word;">
                    <a href="`+element.singleEventUrl+`" class="font-medium text-blue-600">Detalle</a>
                    <a href="#" onclick="_eventsDelete(AT,'`+element.deleteEventUrl+`','`+element.calendarId+`','`+element.id+`')" class="font-medium text-blue-600">Eliminar</a>
                </td>
            </tr>
            `;
        });
        _render_new_element('append','eventsTableBody',contentToAppend);
    }
    if(jsonResponse.hasOwnProperty("removed") && jsonResponse.removed){
        _delete_element('tr_'+jsonResponse.eventId,'delete');
        _toast_statusMessage('success',jsonResponse.message);
    }
}
