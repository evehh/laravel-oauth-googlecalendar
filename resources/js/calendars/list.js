_calendarList = function(accessToken,route){
    let method = "POST";
    let url = route;
    let data = {};
    data["accessToken"] = accessToken;
    data["type"] = 'web';
    let xhrParams = {
    "method":method,
    "url":url,
    "data":data
    };
    _custom_XHRexec(xhrParams,true);
}
_custom_successXHR_execution = function(jsonResponse){
    let contentToAppend = ``;
    _delete_element('calendarsTableBody','clear');
    if(jsonResponse.calendars.length > 0){
        jsonResponse.calendars.forEach((element) => {
            contentToAppend += `
            <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                <td class="px-6 py-4">`+element.id+`</td>
                <td class="px-6 py-4">`+element.summary+`</td>
                <td class="px-6 py-4">`+element.summaryOverride+`</td>
                <td class="px-6 py-4">
                    <a href="`+element.singleCalendarUrl+`" class="font-medium text-blue-600">Ver eventos</a>
                </td>
            </tr>
            `;
        });
        _render_new_element('append','calendarsTableBody',contentToAppend);
    }
}
