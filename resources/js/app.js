require('./bootstrap');
require('./_custom');
require('./_toast');
require('./ui_custom_actions/_render_elements');
window.Swal = require('sweetalert2');
import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();
