_custom_redirect = function(targetURL, method = false) {
    if (method === "replace") {
        // similar behavior as an HTTP redirect
        window.location.replace(targetURL);
    } else {
        // similar behavior as clicking on a link
        window.location.href = targetURL;
    }
}

_custom_successXHR = function(type = 'regular', msg = 'Acción realizada exitosamente.',customFunction = null){
    switch (type) {
        case 'regular':
            _toast_statusMessage('success',msg);
            break;
        case 'function':
            if(customFunction != null){
                customFunction();
            }else{
                _toast_statusMessage('success',msg);
            }
            break;
        default:
            _toast_statusMessage('success',msg);
            break;
    }
}

_custom_getPageToken = function() {
    return document
        .querySelector('meta[name = "csrf-token"]')
        .getAttribute("content");
}

_custom_XHRexec = function(jsonParams, blockUI = false) {
    _loading_blockUI(blockUI);
    let formData = new FormData();
    formData.append("_token", _custom_getPageToken());
    if(jsonParams.laravelFormMethod != null){
        formData.append("_method", jsonParams.laravelFormMethod);
    }
    if(jsonParams.data != null){
        let newData;
        try {
            newData = JSON.parse(jsonParams.data);
        } catch (e) {
            newData = jsonParams.data;
        }
        try{
            for (var prop in newData) {
                formData.append(prop,newData[prop]);
            }
        } catch (e){
        }
    }

    let xhr = new XMLHttpRequest();
    xhr.onload = function () {
        let jsonResponse = null;
        let parseOK = true;
        console.log(xhr);
        if(xhr.responseText){
            try {
                jsonResponse = JSON.parse(xhr.responseText);
            } catch(e) {
                console.log('Response couldn\'t be parsed.');
                parseOK = false;
            }
        }
        if (xhr.status >= 200 && xhr.status < 300 && parseOK) {
            if(typeof(_custom_successXHR_execution) !== 'undefined'){
                if(jsonResponse.success){
                    _loading_unblockUI(blockUI);
                    _custom_successXHR();
                }else{
                    _loading_unblockUI(blockUI);
                    _custom_successXHR_execution(jsonResponse);
                }
            }else{
                _loading_unblockUI(blockUI);
                _custom_successXHR();
            }
        } else {
            if (xhr.status == 403) {
                _loading_unblockUI(blockUI);
                _toast_statusMessage('error','No autorizado. Redirigiendo a la página de inicio.');
                setTimeout(() => {
                    redirect("replace", "/login");
                }, 3000);

            } else if (xhr.status == 419) {
                _loading_unblockUI(blockUI);
                _toast_statusMessage('warning','Página caducada. Actualizando página.');
                setTimeout(() => {
                    location.reload();
                }, 3000);
            } else {
                if(jsonResponse && jsonResponse.hasOwnProperty('errors')){
                    if(jsonResponse.type == 'toast'){
                        let strError = ``;
                        jsonResponse.errors.forEach( (element,index) => {
                            if(index > 0){
                                strError += '\n';
                            }
                            strError += element;
                        });
                        _loading_unblockUI(blockUI);
                        _toast_statusMessage('error',strError);
                    }else if(jsonResponse && jsonResponse.hasOwnProperty("message")){
                        _loading_unblockUI(blockUI);
                        _toast_statusMessage('error',jsonResponse.message);
                    }else{
                        console.log(jsonResponse);
                        _loading_unblockUI(blockUI);
                        _toast_statusMessage('error','Ha ocurrido un error inesperado.');
                    }
                }else if(jsonResponse && jsonResponse.hasOwnProperty('error')){
                    if(jsonResponse.type = 'toast'){
                        _loading_unblockUI(blockUI);
                        _toast_statusMessage('error',jsonResponse.error);
                    }
                }else{
                    _loading_unblockUI(blockUI);
                    _toast_statusMessage('error','Ha ocurrido un error inesperado. Intente nuevamente.');
                }

            }
        }
    };

    xhr.ontimeout = function (e) {
        _toast_statusMessage('warning','Tiempo de espera agotado. Actualice la página.');
        _loading_unblockUI(blockUI);
    };

    xhr.open(jsonParams.method, jsonParams.url, true);
    xhr.setRequestHeader("TYPE-XHR", "true");
    xhr.setRequestHeader("Accept", "application/json");
    if(jsonParams.timeout){
        xhr.timeout = jsonParams.timeout;
    }else{
        xhr.timeout = 10000;
    }

    xhr.send(formData);
}

_custom_logoutFormTrigger = function(){
    let form = document.getElementById("logoutForm");
    if(form){
        form.submit();
    }
}
