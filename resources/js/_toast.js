//type: success|error|info|warning

const { default: Swal } = require("sweetalert2");

//title: string
_toast_statusMessage = function(type,title) {
    let toastMixin = Swal.mixin({
        toast: true,
        icon: type,
        title: title,
        position: 'top-right',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        showCloseButton: true,
        didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });
    toastMixin.fire();
}


_loading_blockUI = function(show = false){
    if(show){
        Swal.mixin({
            html: `<div class="align-items-center">
            <div class="spinner-border spinner-border-sm custom_swal_spinner_margins" role="status" aria-hidden="true" style=""></div>
            <strong>Cargando...</strong></div>`,
            showConfirmButton: false,
            customClass: 'custom_swal_wide',
            backdrop: "true",
            allowOutsideClick: () => {
                const popup = Swal.getPopup()
                popup.classList.remove('swal2-show')
                setTimeout(() => {
                popup.classList.add('animate__animated', 'animate__headShake')
                })
                setTimeout(() => {
                popup.classList.remove('animate__animated', 'animate__headShake')
                }, 500)
                return false
            }
        }).fire();
    }
}

_loading_unblockUI = function(show = false){
    if(show){
        Swal.clickCancel();
    }
}

