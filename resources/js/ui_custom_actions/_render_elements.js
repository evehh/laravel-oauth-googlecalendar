//type:
//------'append': añade el elemento al final de todos los elementos existentes en el contenedor
//------'replaceContent': remplaza todo el contenido existente en el contenedor por el elemento a añadir
//target: id del elemento contenedor en el cual será renderizado el nuevo elemento
//content: contenido a renderizar
//position:
//------'beforebegin': Before the targetElement itself.
//------'afterbegin': Just inside the targetElement, before its first child.
//------'beforeend': Just inside the targetElement, after its last child.
//------'afterend': After the targetElement itself.
_render_new_element = function(type,target,content, position = 'beforeend'){
    let targetContainer = document.getElementById(target);
    if(targetContainer){
        switch (type) {
            case 'append':
                targetContainer.insertAdjacentHTML(position,content);
                break;
            case 'replaceContent':

                break;

            default:
                break;
        }
    }
}

//target: id del elemento contenedor en el cual será renderizado el nuevo elemento
//type (optional):
//------delete: elimina el elemento
//------hide: aplica 'display: none;' al elemento
//------clear: elimina el contenido del elemento
_delete_element = function(target,type = ''){
    let targetElement = document.getElementById(target);
    if(targetElement){
        switch (type) {
            case 'delete':
                targetElement.remove();
                break;
            case 'hide':
                targetElement.style.display = 'none';
                break;
            case 'clear':
                targetElement.innerHTML = '';
                break;
            default:
                targetElement.remove();
                break;
        }
    }
}
