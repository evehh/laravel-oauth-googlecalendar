const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
.js('resources/js/calendars/list.js', 'public/js/calendars/list.js')
.js('resources/js/calendars/events/list.js', 'public/js/calendars/events/list.js')
.js('resources/js/calendars/events/add.js', 'public/js/calendars/events/add.js')
.js('resources/js/calendars/events/details.js', 'public/js/calendars/events/details.js')
.postCss('resources/css/app.css', 'public/css', [
    require('postcss-import'),
    require('tailwindcss'),
    require('autoprefixer'),
])
.version();

