# Guía de despliegue de sistema

## Pre requisitos

-   Servidor de aplicaciones Apache2 o NginX con las extensiones estándar para un proyecto Laravel.
-   Motor de BD PostgreSQL (recomendado), MySQL u otro.
-   Proyecto y clientID en Google Cloud Platform. Se debe habilitar la API Google Calendar. [Crear proyecto Google Cloud.](https://console.cloud.google.com/projectcreate)
-   Gestor de paquetes Composer.

## ¿Cómo desplegar la aplicación?

-   Clonar el repositorio en la carpeta correspondiente a su servidor de aplicaciones.

```
git clone https://gitlab.com/evehh/laravel-oauth-googlecalendar.git
```

<br>

-   Instalar los paquetes necesarios mediante Composer

```
composer install
```

<br>

-   Establecer las variables utilizadas para la conexión con Google en el archivo .env

    -   GOOGLE_CLIENT_ID
    -   GOOGLE_CLIENT_SECRET
    -   GOOGLE_OAUTH_REDIRECT
        <br>

-   Establecer las variables utilizadas por la base de datos en el archivo .env

    -   DB_CONNECTION
    -   DB_HOST
    -   DB_PORT
    -   DB_DATABASE
    -   DB_USERNAME
    -   DB_PASSWORD
        <br>

-   Ejecutar la migración

```
php artisan migrate
```

## Documentación API

Para acceder a la documentación de la API, [haga click aquí.](https://laraveloauthgooglecalendar.docs.apiary.io/)
