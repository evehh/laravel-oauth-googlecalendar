<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\APIGoogleCalendarController;

Route::post('/calendars',[APIGoogleCalendarController::class,'listCalendars'])
            ->name('api_calendars');

Route::post('/events',[APIGoogleCalendarController::class,'listEvents'])
            ->name('api_events');

Route::post('/event',[APIGoogleCalendarController::class,'event'])
            ->name('api_event');

Route::post('/events/add',[APIGoogleCalendarController::class,'addEvent'])
            ->name('api_events_add');

Route::post('/events/update',[APIGoogleCalendarController::class,'updateEvent'])
            ->name('api_events_update');

Route::post('/events/delete',[APIGoogleCalendarController::class,'removeEvent'])
            ->name('api_events_delete');

