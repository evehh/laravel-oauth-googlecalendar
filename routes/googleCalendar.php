<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Auth\VerifyEmailController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\GoogleCalendarController;

Route::middleware('auth')->group(function () {
    Route::get('/calendars', [GoogleCalendarController::class, 'index'])
                ->name('calendars');

    Route::get('/events/{calendarId}', [GoogleCalendarController::class, 'events']);

    Route::get('/events/{calendarId}/add', [GoogleCalendarController::class, 'eventAdd']);

    Route::get('/events/{calendarId}/{eventId}', [GoogleCalendarController::class, 'event']);


});
