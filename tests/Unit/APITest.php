<?php

namespace Tests\Unit;

use Tests\TestCase;

class APITest extends TestCase
{
    public $accessToken = '{"access_token":"ya29.A0ARrdaM-lgjsSsfOiMfoIfAXCNDqn4P2l6eVZ3iayQOp0Lecg-T5fwvspQ7hKCIYnrXY9pBugZRlmu2jZfrnkL04bzGBTWP2lxlP7V2H5ur8O_o_mdysI8Cf8i_eZfwDbXlPAW3Z_nhv4IfaJwddfvmjnagRy","refresh_token":"1//0hGwRpjPCcS44CgYIARAAGBESNgF-L9IrsF1PVhvNG_9M1vYt5nJM6AZrGmsfrjbH4L31RI8y9_GFqSfT5oVLfducF7goGv5SHw","expires_in":3599}';
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_api_list_calendars()
    {
        $response = $this->postJson('/api/calendars', [
            'accessToken' => $this->accessToken,
            'type' => 'api'
        ]);

        $response->assertJson([
                'status' => "success",
            ]);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_api_list_calendar_events()
    {
        $response = $this->postJson('/api/events', [
            'accessToken' => $this->accessToken,
            'type' => 'api',
            'calendarId' => 'primary'
        ]);

        $response->assertJson([
                'status' => "success",
            ]);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_api_get_calendar_event()
    {
        $response = $this->postJson('/api/event', [
            'accessToken' => $this->accessToken,
            'type' => 'api',
            'calendarId' => 'primary',
            'eventId' => '5dsdim1uaecupqfgcvlcsrhhid'
        ]);

        $response->assertJson([
                'status' => "success",
            ]);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_api_add_calendar_event()
    {
        $response = $this->postJson('/api/events/add', [
            'accessToken' => $this->accessToken,
            "type" => "api",
            "calendarId" =>"primary",
            "summary" =>"resumen del evento",
            "description" =>"descripcion del evento",
            "startDateTime" =>"2022-04-01T13:00",
            "endDateTime" =>"2022-04-01T14:00",
        ]);

        $response->assertJson([
                'status' => "success",
            ]);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_api_update_calendar_event()
    {
        $response = $this->postJson('/api/events/update', [
            'accessToken' => $this->accessToken,
            "type" => "api",
            "calendarId" =>"primary",
            "eventId" => "vff25l3p19oa9bn2ab3dabi1hg",
            "summary" =>"evento_modificado_por_tests",
            "description" =>"descripcion del evento_modificado_por_tests",
            "startDateTime" =>"2022-04-01T13:00"
        ]);

        $response->assertJson([
                'status' => "success",
            ]);
    }
}
